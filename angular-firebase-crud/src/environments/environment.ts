// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyCfq_iex27_yN3Ez4u8IryYMMcodI-DGPA",
    authDomain: "angular-firebase-8776b.firebaseapp.com",
    databaseURL: "https://angular-firebase-8776b.firebaseio.com",
    projectId: "angular-firebase-8776b",
    storageBucket: "angular-firebase-8776b.appspot.com",
    messagingSenderId: "885871954048",
    appId: "1:885871954048:web:34585737ccf710cd2d7e65"
 
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
