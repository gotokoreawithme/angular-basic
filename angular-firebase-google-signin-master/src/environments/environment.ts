// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyDPuMjUo7_DhOg04G3w_Gart2o1XISTxOI",
    authDomain: "angular-demo-ed21b.firebaseapp.com",
    databaseURL: "https://angular-demo-ed21b.firebaseio.com",
    projectId: "angular-demo-ed21b",
    storageBucket: "angular-demo-ed21b.appspot.com",
    messagingSenderId: "224743042734",
    appId: "1:224743042734:web:a3616a135641aac64f473f"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
